﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 注意更改路劲
/// </summary>
public class GameUtils 
{
    public static string APP_PATH = @"D:\Program Files (x86)\DeskTop\Demo";// Application.streamingAssetsPath;

    public const string NET_PATH = "http://192.168.1.126:88/jingcha.jpg";
    /// <summary>
    /// 截图并保存
    /// </summary>
    /// <returns></returns>
    public static IEnumerator GetImageByte(Transform canvas,RawImage myImage,RectTransform frameBG,string name ="MyPhoto")
    {
        if (!System.IO.Directory.Exists(APP_PATH)) System.IO.Directory.CreateDirectory(APP_PATH);
         canvas.GetComponent<CanvasGroup>().alpha = 0;//截图前隐藏截图框
        yield return new WaitForEndOfFrame();
        Texture2D newTex = new Texture2D((int)frameBG.sizeDelta.x, (int)frameBG.sizeDelta.y, TextureFormat.ARGB32, false);

        newTex.ReadPixels(new Rect(Screen.width / 2 - 300, Screen.height / 2 - 300, (int)frameBG.sizeDelta.x, (int)frameBG.sizeDelta.y), 0, 0, false);
        newTex.Apply();
        byte[] bytes = newTex.EncodeToPNG();
        string fileName = APP_PATH + "/"+ name + ".png";
        System.IO.File.WriteAllBytes(fileName, bytes); 
        myImage.texture = newTex;
        myImage.SetNativeSize();
        canvas.GetComponent<CanvasGroup>().alpha = 1; 
    }
    /// <summary>
    /// base64编码文本转换成Texture
    /// </summary>

    public static Texture2D Base64ToTexter2d(string Base64STR)
    {
        Texture2D pic = new Texture2D(200, 200);
        byte[] data = System.Convert.FromBase64String(Base64STR);
        //File.WriteAllBytes(Application.dataPath + "/BuildImage/Base64ToSaveImage.png", data);
        pic.LoadImage(data);
        return pic;
    }
    /// <summary>
    /// 图片转换成base64编码文本
    /// </summary>
    public static string ImgToBase64String(string path)
    { 
        FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
        byte[] buffer = new byte[fs.Length];
        fs.Read(buffer, 0, (int)fs.Length);
        string base64String = Convert.ToBase64String(buffer);
        //    Debug.Log("获取当前图片base64为---" + base64String);
        return base64String;
    }
    public static string ImgToBase64String(Texture2D texture2D)
    {
        byte[] buffer = duplicateTexture(texture2D); 
        string base64String = Convert.ToBase64String(buffer); 
        return base64String;
    }
    //如果你想复制一张图片 就把注释打开 更改返回值就可以了
    public static byte[] duplicateTexture(Texture2D source)
    {
        RenderTexture renderTex = RenderTexture.GetTemporary(
                    source.width,
                    source.height,
                    0,
                    RenderTextureFormat.Default,
                    RenderTextureReadWrite.Linear);

        Graphics.Blit(source, renderTex);
        RenderTexture previous = RenderTexture.active;
        RenderTexture.active = renderTex;
        Texture2D readableText = new Texture2D(source.width, source.height);
        readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
        readableText.Apply(); 
        byte[] bytes = readableText.EncodeToJPG();
        return bytes;
      
    }

}


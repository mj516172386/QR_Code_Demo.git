using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
namespace GameFrameWork
{
    public class UtilsJson<T>
    {
        //Json数据写入
        public static void WriteJsonConfigFile(string path, string fileName, T configInfo)
        {
            string file = System.IO.Path.Combine(path, fileName);
            if (!File.Exists(file))
            {
                File.CreateText(file).Dispose();
            }

            string data = JsonUtility.ToJson(configInfo, true);
            Debug.Log("数据写入 data=  " + data);
            File.WriteAllText(file, data);

        }
        //读取JSON内容
        public static T ReadJsonConfigFile(string path, string fileName)
        {
            string file = System.IO.Path.Combine(path, fileName);
            T ConfigInfo = default(T);
            if (File.Exists(file))
            {
                string data = File.ReadAllText(file);
                ConfigInfo = JsonUtility.FromJson<T>(data);
            }
            return ConfigInfo;
        }
        //使用.NET框架中的某Dll文件写入JSON单条数据
        public static void UseNetFrameWriteJsonConfigFile(T course, string path, string fileName)
        {
            string file = System.IO.Path.Combine(path, fileName);
            if (!File.Exists(file))
            {
                File.CreateText(file).Dispose();
            }
            string jsondata = JsonConvert.SerializeObject(course);
            File.WriteAllText(file, jsondata);
        }
        //使用.NET框架中的某Dll文件写入List多条数据到Json文件
        public static void UseNetFrameWriteJsonConfigFiles(List<T> course, string path, string fileName)
        {
            string file = System.IO.Path.Combine(path, fileName);
            if (!File.Exists(file))
            {
                File.CreateText(file).Dispose();
            }
            string jsondata = JsonConvert.SerializeObject(course);
            File.WriteAllText(file, jsondata);
        }
        //使用.NET框架中的某Dll文件读取JSON
        public static T UseNetFrameReadJsonConfigFile(string path, string fileName)
        {
            string file = System.IO.Path.Combine(path, fileName);
            T ConfigInfo = default(T);
            if (File.Exists(file))
            {
                string data = File.ReadAllText(file);
                ConfigInfo = JsonConvert.DeserializeObject<T>(data);
            }
            return ConfigInfo;
        }
        //使用.NET框架中的某Dll文件读取JSON
        public static List<T> UseNetFrameReadJsonConfigFiles(string path, string fileName)
        {
            string file = System.IO.Path.Combine(path, fileName);
            List<T> ConfigInfo = new List<T>();
            if (File.Exists(file))
            {
                string data = File.ReadAllText(file);
                ConfigInfo = JsonConvert.DeserializeObject<List<T>>(data);
            }
            return ConfigInfo;
        }
    }
}
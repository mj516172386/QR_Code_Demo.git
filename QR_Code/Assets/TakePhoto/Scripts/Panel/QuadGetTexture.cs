﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;   

public class QuadGetTexture : MonoBehaviour
{ 
    private WebCamTexture webCamTexture;//摄像头的内容 
    void Start()
    {
        
        //打开了摄像头
        WebCamDevice[] devices = WebCamTexture.devices;
        string deviceName = devices[0].name;
        webCamTexture = new WebCamTexture(deviceName, 400, 300);
        GetComponent<Renderer>().material.mainTexture = webCamTexture;
        webCamTexture.Play();  
    } 
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using ZXing.QrCode.Internal;
using ZXing;
using System.Text;

public class StartPhoto : MonoBehaviour
{
    public Button startBtn;
    public Text countdownText;
    public RawImage showRawImage;
    public RectTransform framebg;
   
    void Start()
    {
        framebg.position = new Vector3(Screen.width / 2 - 300, Screen.height / 2 - 300, 0);
        framebg.gameObject.SetActive(false);
        countdownText.transform.localScale = Vector3.one*0;
        startBtn.onClick.AddListener(() => {
            startBtn.gameObject.SetActive(false);
            StartCoroutine(IEChageText());
        });
    }
    private IEnumerator IEChageText()
    {
        #region 截图保存本地
        for (int i = 0; i < 5; i++)
        {
            countdownText.transform.localScale = Vector3.one ;
            countdownText.text=(5-i).ToString();
            countdownText.transform.DOScale(Vector3.one * 0, 1f);
            yield return new WaitForSeconds(1f);
        }
        framebg.gameObject.SetActive(true);
        //截图保存本地
        StartCoroutine(GameUtils.GetImageByte(transform.parent, showRawImage, framebg));
        #endregion
        //生成显示二维码 RawImage showRawImage  
        Invoke("CreatQRCode",0.5f);
    }
    private void CreatQRCode() 
    {
        var t = GetQRCode.CreatQRCode(GameUtils.NET_PATH, 256, 256);
        showRawImage.texture = t;
        showRawImage.SetNativeSize();
    }
    void Update() 
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            CreatQRCode();
        }
    } 
   
}
